# Day 3 - GitOps
Anthelme Buchaille

### kibana & elastic
La ressource elastic marche, kibana aussi.
Pour kibana, tls obligatoire.
Kibana créé automatiquement un service, que l'on peut renseigner dans l'ingress afin d'y acceder depuis l'exterieur.

```sh
GET _cat/health?v
epoch      timestamp cluster status node.total node.data shards pri relo init unassign pending_tasks max_task_wait_time active_shards_percent
1701246359 08:25:59  elastic green           1         1     10  10    0    0        0             0                  -                100.0%

GET _cat/allocation?v
shards disk.indices disk.used disk.avail disk.total disk.percent host        ip          node
    10       41.2mb    61.5mb    911.8mb    973.4mb            6 10.30.4.182 10.30.4.182 elastic-es-default-0
```

On scale up le cluster elk en changeant "count" de 1 a 3, on vois bien les nouveaux pods se créer (c long)

```sh
GET _cat/health?v
epoch      timestamp cluster status node.total node.data shards pri relo init unassign pending_tasks max_task_wait_time active_shards_percent
1701246909 08:35:09  elastic green           3         3     20  10    2    0        0             0                  -                100.0%

GET _cat/allocation?v
shards disk.indices disk.used disk.avail disk.total disk.percent host        ip          node
     6       38.7mb    55.4mb    917.9mb    973.4mb            5 10.30.7.138 10.30.7.138 elastic-es-default-2
     7        5.3mb    22.5mb    950.8mb    973.4mb            2 10.30.6.211 10.30.6.211 elastic-es-default-1
     7       40.9mb    60.3mb      913mb    973.4mb            6 10.30.4.182 10.30.4.182 elastic-es-default-0
```
> on vois bien que les shards sont bien repartis sur les 3 noeuds.

#### verification que ça marche en allant sur l'interface kibana:

```sh
# Créér l'index tp et ajouter un doc
PUT tp/_doc/1
{
  "body": "hello"
}



{
  "_index" : "tp",
  "_type" : "_doc",
  "_id" : "1",
  "_version" : 1,
  "result" : "created",
  "_shards" : {
    "total" : 2,
    "successful" : 1,
    "failed" : 0
  },
  "_seq_no" : 0,
  "_primary_term" : 1
}

GET tp/_doc/1
{
  "_index" : "tp",
  "_type" : "_doc",
  "_id" : "1",
  "_version" : 1,
  "_seq_no" : 0,
  "_primary_term" : 1,
  "found" : true,
  "_source" : {
    "body" : "hello"
  }
}

GET _cat/indices?v
health status index                           uuid                   pri rep docs.count docs.deleted store.size pri.store.size
green  open   .geoip_databases                NTMCxgI3SG-kGigIi7M47w   1   1         41            0     76.6mb         38.3mb
green  open   .security-7                     ZfDbEWKTTnabRezLeU_Ivw   1   1         53            0    555.5kb        277.7kb
green  open   .apm-custom-link                kSG6G1RhSGuDlPDl_phPUA   1   1          0            0       452b           226b
green  open   .kibana_task_manager_7.16.0_001 xV6-75zGQ7a0ZgPvD4DVQw   1   1         18          124    713.9kb        408.1kb
green  open   .apm-agent-configuration        r5XREN2eSni_e_wMAP3DWQ   1   1          0            0       452b           226b
green  open   .kibana_7.16.0_001              fkK6f7QpRAqVoE8Qzyv7IQ   1   1         32           27        7mb          2.3mb
green  open   tp                              KehdWBkDRUCwNgP_s6ph0g   1   1          1            0      7.6kb          3.7kb
```
> Le document est bien créé dans l'index tp.

### Bonus 1: TODO

### Bonus 2:

```yml
metadata:
  name: {{ .Values.name }}-api-deployment
  annotations:
    checksum/config: {{ include (print $.Template.BasePath "/api-config.yaml") . | sha256sum }}

metadata:
  name: {{ .Values.name }}-front
  labels:
    app: front
  annotations:
    checksum/config: {{ include (print $.Template.BasePath "/front-config.yaml") . | sha256sum }}
```
> Le checksum est calculé lors de l'install/upgrade, et la config est donc update on-the-go dans l'api / le front lorsqu'elle est modifiée.

#### bonus 3:
Fichier values.staging.yaml:
```yaml
name: cdb-staging

api:
  ingress:
    host: api-staging.anthelme-buchaille.takima.school

front:
  ingress:
    host: front-staging.anthelme-buchaille.takima.school
```
> avec cette surcouche du fichier values, on peux deshormais changer l'url de prod a staging, au besoins. Le changement de name permet d'éviter d'avoir des ressources avec le meme nom sur deux déployments.


### GitOps - ArgoCD:

```sh
❯ kubectl get all
NAME                                     READY   STATUS    RESTARTS   AGE
pod/cdb-api-deployment-d69dcbfdc-bdknp   1/1     Running   0          3m29s
pod/cdb-front-7df5dc5c79-dbjmm           1/1     Running   0          3m29s
pod/cdb-pg-0                             1/1     Running   0          49m
pod/cdb-pg-1                             1/1     Running   0          49m

NAME                      TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
service/cdb-api-service   ClusterIP   172.20.248.242   <none>        80/TCP     3m29s
service/cdb-front         ClusterIP   172.20.20.233    <none>        80/TCP     3m29s
service/cdb-pg            ClusterIP   172.20.124.253   <none>        5432/TCP   49m
service/cdb-pg-config     ClusterIP   None             <none>        <none>     49m
service/cdb-pg-repl       ClusterIP   172.20.22.143    <none>        5432/TCP   49m

NAME                                 READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/cdb-api-deployment   1/1     1            1           3m29s
deployment.apps/cdb-front            1/1     1            1           3m29s

NAME                                           DESIRED   CURRENT   READY   AGE
replicaset.apps/cdb-api-deployment-d69dcbfdc   1         1         1       3m29s
replicaset.apps/cdb-front-7df5dc5c79           1         1         1       3m29s

NAME                      READY   AGE
statefulset.apps/cdb-pg   2/2     49m

NAME                                  SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/logical-backup-cdb-pg   30 00 * * *   False     0        <none>          49m

NAME                              TEAM   VERSION   PODS   VOLUME   CPU-REQUEST   MEMORY-REQUEST   AGE     STATUS
postgresql.acid.zalan.do/cdb-pg   cdb    14        2      1Gi                                     3m29s   CreateFailed
Error from server (Forbidden): operatorconfigurations.acid.zalan.do is forbidden: User "system:serviceaccount:tech:anthelme-buchaille" cannot list resource "operatorconfigurations" in API group "acid.zalan.do" in the namespace "anthelme-buchaille"
Error from server (Forbidden): postgresteams.acid.zalan.do is forbidden: User "system:serviceaccount:tech:anthelme-buchaille" cannot list resource "postgresteams" in API group "acid.zalan.do" in the namespace "anthelme-buchaille"
```
> On vois ici (sauf pour le postgres ou on a malheureusement pas les droits de get) que tout est bien créé. on le vois mieux sur l'interface argoCD

#### Pour vérifier que tout fonctionne, essayez de détruire un deployment manuellement dans votre Cluster. Que se passe-t-il ?
```sh
❯ kubectl delete deployment cdb-api-deployment
deployment.apps "cdb-api-deployment" deleted

❯ kubectl get deployments
NAME                 READY   UP-TO-DATE   AVAILABLE   AGE
cdb-api-deployment   1/1     1            1           31s
cdb-front            1/1     1            1           5m49s
```
> Le deployment est recréé automatiquement par argocd.

#### Essayez de modifier le values.yaml en augmentant le replicaCount par exemple. Que se passe-t-il ?
> On modifie le nombre de replicas dans le values.yaml, on push, et on vois que le nombre de replicas est bien modifié dans le cluster.

```sh
❯ kubectl get deployments
NAME                 READY   UP-TO-DATE   AVAILABLE   AGE
cdb-api-deployment   1/1     1            1           8m46s
cdb-front            3/3     3            3           14m
```
> on vois que le nombre de replicas a bien été modifié, car argocd s'est sync et a appliqué les modifications.

#### Bonus:
Le second déployment (staging) fonctionne en meme temps que le 1er, sans conflit, le tout en tls.